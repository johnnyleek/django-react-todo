import React, { useState, useEffect } from 'react';
import styles from './styles/home.module.css';
import utilStyles from './styles/util.module.css';

import ListItem from './components/ListItem';
import Error from './components/Error';
import { getCookie, leadingZero } from './utils';

function App() {
  const [itemInput, setItemInput] = useState("");
  const [itemData, setItemData] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const setError = message => {
    setErrorMessage(message);

    setTimeout(message => {
      setErrorMessage("");
    }, 3000);
  }

  useEffect(() => {
    fetch('/todo/get_items')
      .then(response => response.json())
      .then(data => setItemData(data["items"]));
  }, [])

  const addItem = (itemName) => {
    if(!itemName.trim()) {
      setError("Item cannot be empty!");
      return;
    }

    let items = [...itemData];

    let today = new Date();

    let item = {
      name: itemName,
      time_created: `${leadingZero(today.getMonth() + 1)}/${leadingZero(today.getDate())}/${today.getFullYear()} at ${today.getHours()}:${today.getMinutes()}`
    }

    items.push(item);
    setItemData(items);
    setItemInput("");

    fetch('/todo/create_item/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': getCookie('csrftoken')
      },
      body: JSON.stringify(item)
    })
  }

  const removeItem = (index) => {
    let items = [...itemData];

    fetch('/todo/delete_item/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': getCookie('csrftoken')
      },
      body: JSON.stringify(items[index])
    })

    items.splice(index, 1);
    setItemData(items);
  }

  return (
    <div id={styles.container}>
      {errorMessage === ""
        ? null
        : <Error message={errorMessage}/>
      }
      <h1>To-do List</h1>
      <h3>You have {itemData.length} items</h3>
      <div>
      <label htmlFor="todo_item" className={styles.item_label}>Item:</label>
      <input name="todo_item"
             id="todo_item"
             type="text"
             value={itemInput}
             onChange={e => { setItemInput(e.target.value) }}
      />
      <button onClick={() => { addItem(itemInput) }} className={`btn btn-primary ${utilStyles.margin_left}`}>Add</button>
      </div>

      <div>
        <ul className="list-group">
        {
          itemData.map( (item, index) => {
            return <ListItem item={item} key={index} onDelete={removeItem} index={index}/>
          })
        }
        </ul>
      </div>

    </div>
  );
}

export default App;
