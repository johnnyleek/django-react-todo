import React from 'react';
import styles from '../styles/error.module.css';

export default function Error({ message }) {

  return (
    <div className={`${styles.alert_container} alert alert-danger`}>
      <strong>Error</strong> {message}
    </div>
  )

}
