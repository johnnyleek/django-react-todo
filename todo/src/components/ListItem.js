import React from 'react';
import styles from '../styles/listitem.module.css';
import utilStyles from '../styles/util.module.css';

export default function ListItem(props) {

  return (
    <li className={`list-group-item ${styles.list_item}`}>
      <div className={styles.list_header}>
        <small>{props.item.time_created}</small>
      </div>
      <div className={utilStyles.flex}>
        {props.item.name}
        <span onClick={() => {props.onDelete(props.index)}} className={styles.list_delete}>&times;</span>
      </div>
    </li>
  );

}
