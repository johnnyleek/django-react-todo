from django.contrib import admin
from .models import TodoItem

# Register your models here.
class TodoItemAdmin(admin.ModelAdmin):

    list_display = ('name', 'description', 'time_created')
    list_filter = ('time_created',)
    search_fields = ['name', 'description']

    date_hierarchy = 'time_created'

admin.site.register(TodoItem, TodoItemAdmin)
