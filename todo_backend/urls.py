from django.urls import path
from .views import *

urlpatterns = [
    path('create_item/', create_item),
    path('get_items/', get_items),
    path('delete_item/', delete_item),
    path('home/', ReactView.as_view())
]
