import json

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.generic.base import TemplateView
from .models import TodoItem

# Create your views here.
class ReactView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(ReactView, self).get_context_data(**kwargs)
        return context

def create_item(request, *args, **kwargs):
    if request.method == "POST":
        try:
            item = json.loads(request.body)
            del item['time_created']
            TodoItem.objects.create(
                user=request.user,
                **item)
            return HttpResponse(status=200)
        except:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=400)

def get_items(request, *args, **kwargs):
    if request.method == "GET":
        try:
            items = TodoItem.objects.filter(user=request.user)
            items_json = []
            for item in items:
                items_json.append(item.to_json())
            return JsonResponse({'items': items_json})
        except:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=400)

def delete_item(request, *args, **kwargs):
    if request.method == "POST":
        try:
            item = json.loads(request.body)
            items = TodoItem.objects.get(pk=item['pk']).delete()
            return HttpResponse(status=200)
        except:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=400)
