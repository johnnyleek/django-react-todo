from datetime import datetime

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class TodoItem(models.Model):
    name = models.CharField(max_length=255, default="")
    time_created = models.DateTimeField(default=timezone.now)
    description = models.CharField(max_length=400, default="")
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    def to_json(self):
        return {
            "pk": self.pk,
            "name": self.name,
            "time_created": self.time_created.strftime("%m/%d/%Y at %H:%M"),
            "description": self.description,
            "user": self.user.username
        }
